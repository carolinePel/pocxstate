import { Machine, actions } from 'xstate'
import { interpret } from 'xstate/lib/interpreter'
const { send, log } = actions

// const loggingMachine = Machine({
//   id: 'logging',
//   context: { count: 42 },
//   initial: 'start',
//   states: {
//     start: {
//       on: {
//         FINISH: {
//           target: 'end',
//           actions: log(
//             (ctx, event) => `count: ${ctx.count}, event: ${event.type}`,
//             'Finish label'
//           ),
//         },
//       },
//     },
//     end: {},
//   },
// })

const playMachine = Machine({
  "initial":"start",
  "states":{
      "start": {
          "onEntry":"logInit",
          "on": {
              "PLAY": "playing",
              "SEEK": "seeking",
              "STOP": "stop"
          }
      },
      "seeking":{
          "onEntry":["logSeeking"],
          "on": {
              "ERROR": "stop",
              "SUCCESS": "playing",
              "STOP": "stop",
              "RESTART": "start"
          }
      },
      "playing": {
          "onEntry":['logPlaying'],
          "on": {
              "TOGGLE": "paused",
              "PAUSE": "paused",
              "STOP": "stop",
              "RESTART": "start"
          }
      },
      "paused": {
          "onEntry":['logPaused'],
          "on": {
              "TOGGLE": "playing",
              "PLAY": "playing",
              "STOP": "stop",
              "RESTART": "start"
          }
      },
      "stop": {
          "type":"final"
      }
  }
},{
  "actions":{
      "logInit": (ctx,ev) => {console.log('initiate:',ev)},
      "logSeeking": (ctx,ev) => {console.log('is seeking')},
      "logPlaying": (ctx, ev) => {console.log('playing')},
      "logPaused": (ctx,ev) => {console.log("pausing")},
  }
})


const service = interpret(playMachine)

// Start the service
service.start();

service.send('SEEK')
service.send('SUCCESS')
// const endState = loggingMachine.transition('start', 'FINISH')

// console.log(endState.actions)
