import React from 'react'
import PropTypes from 'prop-types'
import './custom_play_button.css'
import '../index.css'

//default props

function CustomPlayButton(props) {
  const { handlePlay } = props

  return (
    <div className="playWrapper" onClick={handlePlay}>
      <p className="playButtonCenter" type="button" title="lecture/pause">
        <span className="invisible">Play</span>
      </p>
    </div>
  )
}

CustomPlayButton.propTypes = {
  handlePlay: PropTypes.func.isRequired,
}
export default CustomPlayButton
