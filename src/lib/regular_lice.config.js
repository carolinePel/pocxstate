import { Machine, actions } from 'xstate'
import { interpret } from 'xstate/lib/interpreter'

const { send, log } = actions

const states = {
    "relaxed":{ 
        "onEntry":"logRelaxed",
        "on":{
            "BIG_HAND_COMING":"panic",
            "GOTCHA":{
                "target": "dead",
            },
        }
    },
    "panic":{
        "onEntry":"logPanic",
        "activities":["screaming"],
        "on":{
            "GOTCHA":{
                "target": "dead",
            },
        },
    },
    "dead": {
        "onEntry":['logAgony'],
        "type":"final"
    }
}
const lice_actions = {
        "logRelaxed": (ctx,ev) => {console.log("I'm relaxed tadadidadam")},
        "logPanic": (ctx,ev) => {console.log("Hiiiii I'm paniqued!")},
        "logAgony": (ctx, ev) => {console.log("I'm being eaten! I'm dying!!! Arghh")},
}
const activities = {
    "activities": {
        "screaming": () => {
        console.log("AAAAAAAAHHHH")
        }
    }
}

const licesMachine = Machine({
    "initial":"relaxed",
    states,
    activities
  })

export const service = interpret(
    licesMachine
    .withConfig({actions: lice_actions}))

