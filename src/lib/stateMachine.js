import { Machine } from 'xstate'
export const playMachine = Machine({
    "initial":"start",
    "states":{
        "start": {
            "onEntry":"logInit",
            "on": {
                "PLAY": "playing",
                "SEEK": "seeking",
                "STOP": "stop"
            }
        },
        "seeking":{
            "onEntry":["logSeeking"],
            "on": {
                "ERROR": "stop",
                "SUCCESS": "playing",
                "STOP": "stop",
                "RESTART": "start"
            }
        },
        "playing": {
            "onEntry":['logPlaying'],
            "on": {
                "TOGGLE": "paused",
                "PAUSE": "paused",
                "STOP": "stop",
                "RESTART": "start"
            }
        },
        "paused": {
            "onEntry":['logPaused'],
            "on": {
                "TOGGLE": "playing",
                "PLAY": "playing",
                "STOP": "stop",
                "RESTART": "start"
            }
        },
        "stop": {
            "type":"final"
        }
    }
  },{
    "actions":{
        "logInit": (ctx,ev) => {console.log('initiate:',ev)},
        "logSeeking": (ctx,ev) => {console.log('is seeking')},
        "logPlaying": (ctx, ev) => {console.log('playing')},
        "logPaused": (ctx,ev) => {console.log("pausing")},
    }
  })
