import { Machine, actions } from 'xstate'
import { interpret } from 'xstate/lib/interpreter'

const { sendParent } = actions

const states = {
    "awake": {
        "onEntry":"logAwake",
        "on": {
            "SLEEPY": "sleeping",
            "THROW_BANANA": "getBanana",
            "ASKING_NICELY":"waiting",
        }
    },
    "waiting":{
        "onEntry":[
            'logWaiting',
            //sendParent("WAITING")
        ],
        "on":{
            "GOTCHA":"awake"
        }
    },
    "sleeping":{
        "onEntry":["logApeSleeping"],
        "on": {
            "DISTURBING": "awake",
            "THROW_BANANA": "awake",
            "ASKING_NICELY":"awake",
            }
    },
    "getBanana": {
        "onEntry":['logBattle'],
        "on": {
            "GOTBANANA":"eat",
            "FAILURE":"awake",
        }
    },
    "eat": {
        "onEntry":['logIndigestion'],
        "type":"final"
    }
}

const regular_actions = {
    "logIndigestion": (ctx,ev) => {console.log("Burp... I don't feel so well:",ev)},
    "logBattle": (ctx,ev) => {console.log('Pif Paf Puf(battle sounds)')},
    "logAwake": (ctx, ev) => {console.log("I'm awake")},
    "logApeSleeping": (ctx,ev) => {console.log("rrrrrr")},
    "logWaiting": (ctx,ev)=>{console.log("I'm waiting for you to delouse me")}
}

export const regular_ape = Machine({
    "id":"regular_ape",
    "initial":"awake",
    states,
  })

export const service = interpret(regular_ape
.withConfig({actions: regular_actions}))