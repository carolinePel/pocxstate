import { service } from './regular_ape.config'

service
    .onTransition(nextState => {
    console.log(nextState.value)
    })
    .start()
    
/**
 * tests
 */

// awake SLEEPY THROW_BANANA ASKING_NICELY
// waiting GOTCHA
// sleeping DISTURBING THROW_BANANA ASKING_NICELY
// getbanana GOTBANANA FAILURE

service.send('ASKING_NICELY')
service.send('GOTCHA')
service.send('SLEEPY')
service.send("ASKING_NICELY")
service.send("THROW_BANANA")
service.send("FAILURE")
service.send('SLEEPY')
service.send("THROW_BANANA")
service.send("THROW_BANANA")
service.send("GOTBANANA")
