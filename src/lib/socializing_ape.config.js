import { Machine, actions } from 'xstate'
import { interpret } from 'xstate/lib/interpreter'
import { regular_ape } from './regular_ape.config'
const { send } = actions


const states = {
    think:{
        onEntry:"logThinking",
        after:{
            1000:[
                {
                    target:"wantsToSocialize",
                    cond: ctx => { 
                        console.log(socialApeMachine.initialState.context)
                        return socialApeMachine.initialState.context.isCharitable 
                    }
                },
                {
                    target:"think",
                    internal:true
                }
            ]
        }
        
    },
    wantsToSocialize: {
        invoke:[regular_ape],
        onEntry:[
            "logSocial",
            send('ASKING_NICELY', {to: regular_ape.id}),
        ],
        on: {
            WAITING:'searchLices',
        },
        onExit: [
            () => {console.log("exit")}
        ]
    },
    searchLices: {
        invoke:[regular_ape],
        onEntry:[
            "logSearch",
            send('BIG_HAND_COMING', {to: regular_ape.id}),
            //(Math.random() < 0.5) ? send('GOTCHA') : send('RETRY')
        ],
        after:{
            3000:'eat'            
        },
        on: {
            GOTCHA:"eat",
            RETRY:"retry"
        }
    },
    retry: {
        onEntry:[
            send("SEARCH")
        ],
        on: {
            SEARCH: "searchLices"
        }
    },
    eat: {
        onEntry:['logEating'],
        type:"final"
    }
}

const social_actions = {
    "logThinking": (ctx,ev) => {console.log("To be or not to be? I'm shApespear eh eh!")},
    "logSocial": (ctx,ev) => {console.log("Do you wanna be my friend?")},
    "logSearch": (ctx, ev) => {console.log("Where are you little lices? shApespear is gonna eat you!")},
    "logEating": (ctx,ev) => {console.log("yum yum! these lices are deLiceous eh eh eh")}
}

const socialApeMachine = Machine({
    initial:"think",
    context:{
        isCharitable:true,
        randomTime: parseInt(Math.random()*10000)
    },
    states
})

export const social_ape_service = interpret(
    socialApeMachine
    .withConfig({ actions:social_actions })
)